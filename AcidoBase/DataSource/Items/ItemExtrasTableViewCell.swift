//
//  ItemExtrasTableViewCell.swift
//  AcidoBase
//
//  Created by Charls Salazar on 11/04/20.
//  Copyright © 2020 tres83. All rights reserved.
//

import UIKit

class ItemExtrasTableViewCell: BaseTableViewCell {
    @IBOutlet weak var mDescriptionLabel: UILabel!
    @IBOutlet weak var mTitleLabel: UILabel!
    
    override func awakeFromNib() {
           super.awakeFromNib()
       }
       
       override func setSelected(_ selected: Bool, animated: Bool) {
           super.setSelected(selected, animated: animated)
       }
       
       override func pupulate(object: NSObject) {
           let mFormula = object as! ValoresNormalesModel
           mDescriptionLabel.text = mFormula.mDescription
           mTitleLabel.text = mFormula.mTitle
       }

       override public func toString() -> String{
           return "ItemExtrasTableViewCell"
       }
}
