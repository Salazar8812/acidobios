
//
//  ValoresNormalesModel.swift
//  AcidoBase
//
//  Created by Charls Salazar on 11/04/20.
//  Copyright © 2020 tres83. All rights reserved.
//

import UIKit

class ValoresNormalesModel: NSObject {
    var mTitle : String?
    var mDescription : String?
    
    init(mTitle : String, mDescription: String){
        self.mTitle = mTitle
        self.mDescription = mDescription
    }
}
