//
//  GlosarioModel.swift
//  AcidoBase
//
//  Created by Charls Salazar on 11/04/20.
//  Copyright © 2020 tres83. All rights reserved.
//

import UIKit

class GlosarioModel: NSObject {
    var mTitleFormula : String?
    var mDescription : String?
    
    init(mTitleFormula : String, mDescription : String){
        self.mTitleFormula = mTitleFormula
        self.mDescription = mDescription
    }
}
