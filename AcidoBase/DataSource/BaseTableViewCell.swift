//
//  BaseTableViewCell.swift
//  AcidoBase
//
//  Created by Charls Salazar on 11/04/20.
//  Copyright © 2020 tres83. All rights reserved.
//

import UIKit

class BaseTableViewCell: UITableViewCell {
    
    public var delegate : NSObjectProtocol!
    public var itemObject : NSObject?
    public var delegateCell: BaseTableDelegate?
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapEdit(_:)))
        addGestureRecognizer(tapGesture)
    }
    
    @objc public func tapEdit(_ sender: UITapGestureRecognizer) {
        delegateCell?.baseTableDelegate(sender: sender)
    }
    
    public func pupulate(object :NSObject) {
        preconditionFailure("This method must be overridden")
    }
    
    public func pupulateSelected(object :NSObject) {
        
    }
    
    public func executeAction() {
        preconditionFailure("This method must be overridden")
    }
    
    public func toString() -> String{
        preconditionFailure("This method must be overridden")
    }
    
}

public protocol BaseTableDelegate {
    func baseTableDelegate(sender: UITapGestureRecognizer)
}

