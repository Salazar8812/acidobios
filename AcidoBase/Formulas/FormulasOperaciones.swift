//
//  FormulasOperaciones.swift
//  AcidoBase
//
//  Created by Charls Salazar on 11/04/20.
//  Copyright © 2020 tres83. All rights reserved.
//

import UIKit

class FormulasOperaciones: NSObject {
    
    private var currencyFormatter: NumberFormatter?
    var mResults : [Double] = []
    var mInput : [String] = []
    
    init(mInput : [String]){
        self.mInput = mInput
        currencyFormatter = NumberFormatter()
        currencyFormatter!.usesGroupingSeparator = true
        currencyFormatter!.numberStyle = .currency
        currencyFormatter!.locale = Locale.current
    }
    
    func form1() {
        let firstResult = mInput[12].toDouble()! * 2
        let secondResult = mInput[11].toDouble()! + firstResult
        mResults[1] = secondResult / 3
        self.currencyFormatter?.string(from: NSNumber(value: Double(mResults[1])))
    }

    func form2() {
        mResults[2] = mInput[11].toDouble()! / mInput[14].toDouble()!
        self.currencyFormatter?.string(from: NSNumber(value: Double(mResults[2])))

    }

    func form3() {
        mResults[3] = mInput[14].toDouble()! / mResults[1]
    }

    func form4() {
        mResults[4] = mInput[7].toDouble()! -  mInput[6].toDouble()!
    }

    func form5() {
        let convertSaO2 = mInput[8].toDouble()! / 100;
        let firstResult = mInput[10].toDouble()! * 1.34 * convertSaO2;
        let secondResult = mInput[4].toDouble()! * 0.0031;
        let convertSvO2 = mInput[9].toDouble()! / 100;
        let thirdResult = mInput[10].toDouble()! * 1.34 * convertSvO2;
        let fourthResult = mInput[5].toDouble()! * 0.0031;
        let fifthResult = firstResult + secondResult;
        let sixth = thirdResult + fourthResult;
        let seventh = fifthResult - sixth;
        mResults[5] = mResults[4] / seventh;
    }

    func form6() {
        let firstResult = mInput[2].toDouble()! * 4;
        let secondResult = firstResult + 7;
        let thirdResult = mInput[2].toDouble()! + 90;
        mResults[6] = secondResult / thirdResult;
    }

    func form7() {
        let convertSaO2 = mInput[8].toDouble()! / 100;
        let firstResult = mInput[10].toDouble()! * 1.34 * convertSaO2;
        let secondResult = mInput[4].toDouble()! * 0.0031;
        mResults[7] = firstResult + secondResult;
    }

    func form8() {
        let convertSaO2 = mInput[9].toDouble()! / 100;
        let firstResult = mInput[10].toDouble()! * 1.34 * convertSaO2;
        let secondResult = mInput[5].toDouble()! * 0.0031;
        mResults[8] = firstResult + secondResult;
    }

    func form9() {
        mResults[9] = mResults[7] - mResults[8];
    }

    func form10() {
        let firstResult = mResults[9] * 100;
        let secondResult = firstResult / mResults[7];
        mResults[10] = secondResult / mResults[9];
    }

    func form11() {
        mResults[11] = mResults[10] / mResults[6];
    }

    func form12() {
        mResults[12] = mResults[7] * mResults[10] * 10;
    }

    func form13() {
        mResults[13] = mResults[12] / mResults[6];
    }

    func form14() {
        mResults[14] = mResults[9] * mResults[10] * 10;
    }

    func form15() {
        mResults[15] = mResults[14] / mResults[6];
    }

    func form16() {
        let firstResult = mInput[8].toDouble()! - mInput[9].toDouble()!
        mResults[16] = firstResult / mInput[8].toDouble()!
    }

    func form17() {
        let firstResult = mResults[10] / mInput[14].toDouble()!
        mResults[17] = firstResult * 1000;
    }

    func form18() {
        let firstResult = mResults[17] / mResults[6];
        mResults[18] = firstResult;
    }

    func form19() {
        mResults[19] = mResults[10] * mResults[1] * 0.0022;
    }

    func form20() {
        mResults[20] = mResults[19] / mResults[6];
    }
}

extension String {
    func toDouble() -> Double? {
        return NumberFormatter().number(from: self)?.doubleValue
    }
}

extension UIImage {
    func tint(with color: UIColor) -> UIImage {
        var image = withRenderingMode(.alwaysTemplate)
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        color.set()
        
        image.draw(in: CGRect(origin: .zero, size: size))
        image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
}

extension Double {
    func toString() -> String {
        return String(format: "%.2f",self)
    }
}

