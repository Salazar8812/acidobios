//
//  FormularioViewController.swift
//  AcidoBase
//
//  Created by Charls Salazar on 07/04/20.
//  Copyright © 2020 tres83. All rights reserved.
//

import UIKit

class FormularioViewController: UIViewController {
    @IBOutlet weak var mHeaderView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
     setBottomCorners()
           }
           
           func setBottomCorners(){
                 mHeaderView.clipsToBounds = true
                 mHeaderView.layer.cornerRadius = 30
                 mHeaderView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
             }
}
