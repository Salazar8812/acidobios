//
//  SplashScreenViewController.swift
//  AcidoBase
//
//  Created by Charls Salazar on 07/04/20.
//  Copyright © 2020 tres83. All rights reserved.
//

import UIKit
import FLAnimatedImage

class SplashScreenViewController: UIViewController {
    var counter = 0
    var timer = Timer()
    
    @IBOutlet weak var mAnimateImage: FLAnimatedImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        delayLaunchIntroductionView()
      //  loadGifsProgress()
    }
    
  
    
    func delayLaunchIntroductionView(){
        timer.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(actionDelayHome), userInfo: nil, repeats: true)
    }
    
    @objc func actionDelayHome() {
        counter += 1
        if(counter == 10){
            timer.invalidate()
            launchHome()
        }
    }
    
    func launchHome(){
        let storyboard : UIStoryboard = UIStoryboard(name: "MainMenu", bundle: nil)
        let vc : MainMenuViewController = storyboard.instantiateViewController(withIdentifier: "MainMenuViewController") as! MainMenuViewController
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }
}
