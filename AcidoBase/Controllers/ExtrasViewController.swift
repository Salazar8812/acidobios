//
//  ExtrasViewController.swift
//  AcidoBase
//
//  Created by Charls Salazar on 11/04/20.
//  Copyright © 2020 tres83. All rights reserved.
//

import UIKit

class ExtrasViewController: UIViewController, TableViewCellClickDelegate {
    @IBOutlet weak var mHeaderView: UIView!
    @IBOutlet weak var mExtrasTableView: UITableView!
    var mDataSource : GlosarioDataSource<NSObject, ItemExtrasTableViewCell>?
    var mValoresNormales : [ValoresNormalesModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mDataSource = GlosarioDataSource(tableView: self.mExtrasTableView, delegate: self)
        mDataSource?.setHeightRow(height:65)
        mDataSource?.update(items: mValoresNormales)
        setBottomCorners()
        populateListGlosario()
        
    }
    
    func populateListGlosario(){
        mValoresNormales.append(ValoresNormalesModel(mTitle: "TAM",mDescription: "63 a 100 mm Hg"))
        mValoresNormales.append(ValoresNormalesModel(mTitle: "Ìndice de choque",mDescription: "menor 0.5 a 0.7"))
        mValoresNormales.append(ValoresNormalesModel(mTitle: "Ìndice de choque modificado",mDescription: "menor 0.7 a 0.13"))
        mValoresNormales.append(ValoresNormalesModel(mTitle: "DvaCO2",mDescription: "menor a 6 mmHg"))
        mValoresNormales.append(ValoresNormalesModel(mTitle: "DvaCO2/DvaO2",mDescription: "1"))
        mValoresNormales.append(ValoresNormalesModel(mTitle: "CaO2 (mL/dL)",mDescription: "17.5 a 23.5 ml/dl"))
        mValoresNormales.append(ValoresNormalesModel(mTitle: "CVO2",mDescription: "12 a 17 ml/dl"))
        mValoresNormales.append(ValoresNormalesModel(mTitle: "DavO2",mDescription: "3.5 a 5.5 ml/dl"))
        mValoresNormales.append(ValoresNormalesModel(mTitle: "GC (L/min)",mDescription: "34 a 8 L/min"))
        mValoresNormales.append(ValoresNormalesModel(mTitle: "VS",mDescription: "60 a 100ml"))
        mValoresNormales.append(ValoresNormalesModel(mTitle: "ISV",mDescription: "33 a 47 ml/m2"))
        mValoresNormales.append(ValoresNormalesModel(mTitle: "RSV",mDescription: "1000  1500 dinas"))
        mValoresNormales.append(ValoresNormalesModel(mTitle: "IRVS",mDescription: "1760 a 2600 dinas/m2"))
        mValoresNormales.append(ValoresNormalesModel(mTitle: "DO2",mDescription: "900 a 1200 ml/min"))
        mValoresNormales.append(ValoresNormalesModel(mTitle: "IDO2",mDescription: "520 a 720 ml/min/m2"))
        mValoresNormales.append(ValoresNormalesModel(mTitle: "VO2",mDescription: "200 a 250 mL/min"))
        mValoresNormales.append(ValoresNormalesModel(mTitle: "IVO2",mDescription: "120 a 160 mL/min/m2"))
        mValoresNormales.append(ValoresNormalesModel(mTitle: "IEO2 (%)",mDescription: "22 a 30 %"))
        mValoresNormales.append(ValoresNormalesModel(mTitle: "IVP",mDescription: ">14%"))
        mValoresNormales.append(ValoresNormalesModel(mTitle: "PVC",mDescription: "0 a 6 mm Hg"))
        mValoresNormales.append(ValoresNormalesModel(mTitle: "PC",mDescription: "0.7 a 1 W"))
        mValoresNormales.append(ValoresNormalesModel(mTitle: "IPC",mDescription: "0.5 a 0.7 W/m2"))

        mDataSource?.update(items: mValoresNormales)
    }
    
    func setBottomCorners(){
        mHeaderView.clipsToBounds = true
        mHeaderView.layer.cornerRadius = 30
        mHeaderView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
    }
    
    func onTableViewCellClick(item: NSObject, cell: UITableViewCell) {
        
    }
}
