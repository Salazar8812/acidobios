//
//  MainMenuViewController.swift
//  AcidoBase
//
//  Created by Charls Salazar on 07/04/20.
//  Copyright © 2020 tres83. All rights reserved.
//

import UIKit

class MainMenuViewController: UIViewController {
    @IBOutlet weak var mHomeButton: UIButton!
    @IBOutlet weak var mFormulasButton: UIButton!
    @IBOutlet weak var mGlosarioButton: UIButton!
    @IBOutlet weak var mExtraButton: UIButton!
    
    @IBOutlet weak var mContainer: UIView!
    @IBOutlet weak var mContentMenuOptionsView: UIView!
    @IBOutlet weak var mExtraLabel: UILabel!
    @IBOutlet weak var mExtraImageView: UIImageView!
    @IBOutlet weak var mGlosarioLabel: UILabel!
    @IBOutlet weak var mGlosarioImageView: UIImageView!
    @IBOutlet weak var mFormulasLabel: UILabel!
    @IBOutlet weak var mFormulasImageView: UIImageView!
    @IBOutlet weak var mHomeLabel: UILabel!
    @IBOutlet weak var mHomeImageView: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setHome()
        addTarget()
        setColorSelected(mImage: mHomeImageView, mTitle: mHomeLabel)
        setColorDefault(mImage: mFormulasImageView, mTitle: mFormulasLabel)
        setColorDefault(mImage: mGlosarioImageView, mTitle: mGlosarioLabel)
        setColorDefault(mImage: mExtraImageView, mTitle: mExtraLabel)
    }
    
    func addTarget(){
        mHomeButton.addTarget(self, action: #selector(homeAction), for: .touchUpInside)
        mFormulasButton.addTarget(self, action: #selector(formulasAction), for: .touchUpInside)
        mGlosarioButton.addTarget(self, action: #selector(glosarioAction), for: .touchUpInside)
        mExtraButton.addTarget(self, action: #selector(extrasAction), for: .touchUpInside)
    }
        
    @objc func homeAction(_ sender : UIButton){
        let news = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        news.view.frame = mContainer.bounds
        mContainer.addSubview(news.view)
        addChild(news)
        news.didMove(toParent: self)
        setColorSelected(mImage: mHomeImageView, mTitle: mHomeLabel)
        setColorDefault(mImage: mFormulasImageView, mTitle: mFormulasLabel)
        setColorDefault(mImage: mGlosarioImageView, mTitle: mGlosarioLabel)
        setColorDefault(mImage: mExtraImageView, mTitle: mExtraLabel)

    }
    
    @objc func formulasAction(_ sender : UIButton){
        let news = self.storyboard?.instantiateViewController(withIdentifier: "FormularioViewController") as! FormularioViewController
        news.view.frame = mContainer.bounds
        mContainer.addSubview(news.view)
        addChild(news)
        news.didMove(toParent: self)
        setColorDefault(mImage: mHomeImageView, mTitle: mHomeLabel)
        setColorSelected(mImage: mFormulasImageView, mTitle: mFormulasLabel)
        setColorDefault(mImage: mGlosarioImageView, mTitle: mGlosarioLabel)
        setColorDefault(mImage: mExtraImageView, mTitle: mExtraLabel)
    }
    
    @objc func glosarioAction(_ sender : UIButton){
        let news = self.storyboard?.instantiateViewController(withIdentifier: "GlosarioViewController") as! GlosarioViewController
        news.view.frame = mContainer.bounds
        mContainer.addSubview(news.view)
        addChild(news)
        news.didMove(toParent: self)
        setColorDefault(mImage: mHomeImageView, mTitle: mHomeLabel)
        setColorDefault(mImage: mFormulasImageView, mTitle: mFormulasLabel)
        setColorSelected(mImage: mGlosarioImageView, mTitle: mGlosarioLabel)
        setColorDefault(mImage: mExtraImageView, mTitle: mExtraLabel)
    }
    
    @objc func extrasAction(_ sender : UIButton){
        let news = self.storyboard?.instantiateViewController(withIdentifier: "ExtrasViewController") as! ExtrasViewController
        news.view.frame = mContainer.bounds
        mContainer.addSubview(news.view)
        addChild(news)
        news.didMove(toParent: self)
        setColorDefault(mImage: mHomeImageView, mTitle: mHomeLabel)
        setColorDefault(mImage: mFormulasImageView, mTitle: mFormulasLabel)
        setColorDefault(mImage: mGlosarioImageView, mTitle: mGlosarioLabel)
        setColorSelected(mImage: mExtraImageView, mTitle: mExtraLabel)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    func setHome(){
        let news = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        news.view.frame = mContainer.bounds
        mContainer.addSubview(news.view)
        addChild(news)
        news.didMove(toParent: self)
    }
    
    
    func setColorDefault(mImage : UIImageView, mTitle: UILabel){
        mTitle.textColor = UIColor.lightGray
        mImage.image = mImage.image?.withRenderingMode(.alwaysTemplate)
        mImage.tintColor = UIColor.lightGray
    }
    
    func setColorSelected(mImage : UIImageView, mTitle: UILabel){
        mTitle.textColor = UIColor(netHex: Colors.color_white)
        mImage.image = mImage.image?.withRenderingMode(.alwaysTemplate)
        mImage.tintColor = UIColor(netHex: Colors.color_white)
    }
    
}
