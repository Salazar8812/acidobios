//
//  GlosarioViewController.swift
//  AcidoBase
//
//  Created by Charls Salazar on 07/04/20.
//  Copyright © 2020 tres83. All rights reserved.
//

import UIKit

class GlosarioViewController: UIViewController, TableViewCellClickDelegate {
    
    
    @IBOutlet weak var mHeaderView: UIView!
    @IBOutlet weak var mGlosarioTableview: UITableView!
    var mDataSource : GlosarioDataSource<NSObject, ItemGlosarioTableViewCell>?
    var mGlosarioList : [GlosarioModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mDataSource = GlosarioDataSource(tableView: self.mGlosarioTableview, delegate: self)
        mDataSource?.setHeightRow(height:65)
               mDataSource?.update(items: mGlosarioList)
        setBottomCorners()
        populateListGlosario()
    }
    
    func setBottomCorners(){
        mHeaderView.clipsToBounds = true
        mHeaderView.layer.cornerRadius = 30
        mHeaderView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
    }
    
    func populateListGlosario(){
        mGlosarioList.append(GlosarioModel(mTitleFormula: "CaO2",mDescription: "Concentración arterial de oxígeno"))
        mGlosarioList.append(GlosarioModel(mTitleFormula: "CvO2",mDescription: "Concentración venosa de oxígeno"))
        mGlosarioList.append(GlosarioModel(mTitleFormula: "Da-vO2",mDescription: "Diferencia arterio-venosa de oxígeno"))
        mGlosarioList.append(GlosarioModel(mTitleFormula: "VO2",mDescription: "Velocidad de consumo de oxígeno"))
        mGlosarioList.append(GlosarioModel(mTitleFormula: "DO2",mDescription: "Disponibilidad de oxígeno"))
        mGlosarioList.append(GlosarioModel(mTitleFormula: "IEO2",mDescription: "Indice de extracción de oxígeno"))
        mGlosarioList.append(GlosarioModel(mTitleFormula: "CO",mDescription: " Gasto cardiaco"))
        mGlosarioList.append(GlosarioModel(mTitleFormula: "DVACO2",mDescription: "Diferencia veno-arterial de dióxido de carbono"))
        mGlosarioList.append(GlosarioModel(mTitleFormula: "DVACO2/DVAO2",mDescription: "Relación de diferencia veno-arterial de dióxido de carbono entre diferencia de artero-venosa de oxígeno"))
        mGlosarioList.append(GlosarioModel(mTitleFormula: "FC",mDescription: "Frecuencía cardiaca"))
        mGlosarioList.append(GlosarioModel(mTitleFormula: "TAS",mDescription: "Tensión arterial sistólica"))
        mGlosarioList.append(GlosarioModel(mTitleFormula: "TAM",mDescription: "Tensión arterial media"))
        mGlosarioList.append(GlosarioModel(mTitleFormula: "VS",mDescription: "Volumen sistólico"))
        mGlosarioList.append(GlosarioModel(mTitleFormula: "IVS",mDescription: "Volumen sistólico indexado"))
        mGlosarioList.append(GlosarioModel(mTitleFormula: "PD",mDescription: "Poder cardiaco"))
        mGlosarioList.append(GlosarioModel(mTitleFormula: "IPD",mDescription: "Poder cardiaco indexado"))

        mDataSource?.update(items: mGlosarioList)
    }
    
    func onTableViewCellClick(item: NSObject, cell: UITableViewCell) {
    
    }
    
    
}
