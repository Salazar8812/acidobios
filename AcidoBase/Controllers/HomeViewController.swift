//
//  HomeViewController.swift
//  AcidoBase
//
//  Created by Charls Salazar on 07/04/20.
//  Copyright © 2020 tres83. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    @IBOutlet weak var mTitleLabel: UILabel!
    @IBOutlet weak var mHeaderBannerView: UIView!
    @IBOutlet weak var mScrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setBottomCorners()
        mTitleLabel.text = "Ingresa los siguientes datos\npara obtener el análisis"
    }
    
    func setBottomCorners(){
        mHeaderBannerView.clipsToBounds = true
        mHeaderBannerView.layer.cornerRadius = 30
        mHeaderBannerView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
    }
    
}
